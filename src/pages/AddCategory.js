import { instance as axios } from "../util/api";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";

function AddCategory() {
  const navigate = useNavigate();

  const [name, setName] = useState("");

  const AddMenus = async () => {
    try {
      // untuk mengirimkan file multipart ke server
      const formData = {
        name: name,
      };

      await axios.post(`/api/categorys/add`, formData);
      Swal.fire({
        icon: "success",
        title: "Good job!",
        text: "Category added successfully",
        showConfirmButton: false,
        timer: 1000,
      });
      navigate("/menus");
    } catch (err) {
      console.log(err);
    }
  };

  const save = (e) => {
    e.preventDefault();
    AddMenus();
  };

  return (
    <div className="container alltop mb-5">
      <h2 className="text-center mb-3">
        <i className="fa-sharp fa-solid fa-utensils"></i> {""}
        <span className="text-danger">Add Category </span>
      </h2>

      <form className="card p-4 mt-4">
        <div className="form-group">
          <label>
            <strong>Name</strong>
          </label>
          <input
            type="text"
            className="form-control"
            placeholder="Title"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>
      </form>

      <button
        onClick={save}
        className="btn btn-block mt-3"
        style={{
          backgroundColor: "#B22222",
          color: "white",
        }}
      >
        Save
      </button>
    </div>
  );
}

export default AddCategory;
