import React from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { convertCurrency } from "../util/util";
import { instance as axios } from "../util/api";

function Menu(props) {
  const { menu } = props;

  const removePost = async () => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/api/menus/delete/${menu.id}`);
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled!",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  return (
    <div className="card h-100 shadow-sm card-menu">
      <img src={menu.image} className="card-img-top" alt="" />
      <div className="label-top shadow-sm">
        <span className="text-white d-flex justify-content-center">
          {menu.discount}%
        </span>
      </div>
      <div className="card-body">
        {localStorage.getItem("role") === "admin" ? (
          <div>
            <div className="d-flex justify-content-center">
              <Link to={`/editMenu/${menu.id}`}>
                <i className="fa-sharp fa-solid fa-pen-to-square text-warning"></i>
              </Link>{" "}
              {""}
              <span>
                <i
                  className="fa-solid fa-trash text-danger px-2"
                  onClick={removePost}
                  style={{ cursor: "pointer" }}
                ></i>
              </span>
            </div>
            <hr className=" hr" />
          </div>
        ) : (
          <div></div>
        )}

        <div className="mb-3">
          <span>{convertCurrency(menu.price, "id-ID", "IDR")}</span>

          <span className="float-end rating">
            <ion-icon name="star"></ion-icon>
            <ion-icon name="star"></ion-icon>
            <ion-icon name="star"></ion-icon>
            <ion-icon name="star"></ion-icon>
          </span>
        </div>
        <h5 className="card-title">
          <p>{menu.name}</p>
        </h5>
        <div className="d-grid gap-2 mt-4 mb-2">
          <Link
            to={`/menus/${menu.id}`}
            className="btn bold-btn"
            style={{
              backgroundColor: "#b22222",
              color: "white",
            }}
          >
            Menu Detail
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Menu;
