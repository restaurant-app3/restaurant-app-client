import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { instance as axios } from "../util/api";

function Login() {
  const location = useLocation();
  const navigate = useNavigate();

  const [userLogin, setUserLogin] = useState({ email: "", password: "" });

  const handleOnChange = (e) => {
    setUserLogin((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  const signIn = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(`/login`, userLogin);

      if (status === 200) {
        localStorage.setItem("token", data.token);
        localStorage.setItem("id", data.userData.id);
        localStorage.setItem("role", data.userData.role);
        localStorage.setItem("email", data.userData.email);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          Swal.fire({
            icon: "success",
            title: "Yeahh!",
            text: "You have successfully logged in",
            showConfirmButton: false,
            timer: 1000,
          });
          navigate("/");
        }
      } else {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "Username or Password Undefined!",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="container alltop mb-4 p-5">
      <div className="card p-5" style={{ border: "none" }}>
        <div className="row d-flex justify-content-center align-items-center">
          <div className="col-lg-6 p-0">
            <img src="images/login.png" style={{ width: "100%" }} />
          </div>
          <div className="col-lg-6 col-xl-4">
            <h3
              className="text-center mb-3 fw-bold"
              style={{ color: "#B22222" }}
            >
              Login
            </h3>
            <form className="card p-4">
              <div className="form-outline mb-3">
                <label className="form-label">Email</label>
                <input
                  type="email"
                  id="email"
                  className="form-control"
                  onChange={handleOnChange}
                  value={userLogin.email}
                  placeholder="Enter email address"
                />
              </div>

              <div className="form-outline mb-3">
                <label className="form-label">Password</label>
                <input
                  type="password"
                  id="password"
                  className="form-control"
                  onChange={handleOnChange}
                  value={userLogin.password}
                  placeholder="Enter password"
                />
              </div>

              <div className="text-center mt-4">
                <button
                  type="button"
                  className="btn btn-color mb-3 w-100 text-white fw-bold"
                  style={{ backgroundColor: "#B22222" }}
                  onClick={signIn}
                >
                  Login
                </button>
                <p className="small fw-bold mt-2 mb-0">
                  Don't have an account ? {""}
                  <Link className="link-danger" to="/register">
                    Register
                  </Link>
                </p>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
