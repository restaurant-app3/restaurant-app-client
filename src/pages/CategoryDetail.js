import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import EmptyMenu from "./EmptyMenu";
import { convertCurrency } from "../util/util";
import { instance as axios } from "../util/api";

function CategoryDetail() {
  const [categorys, setCategorys] = useState([]);
  const { id } = useParams();

  const fetchCategoryId = async () => {
    try {
      const { data, status } = await axios.get(`/api/categorys/${id}`);
      if (status === 200) {
        setCategorys(data.menu);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchCategoryId();
  }, []);

  const deleteMenu = async () => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/api/menus/delete/${id}`);
        Swal.fire("Deleted!", "Successfully deleted your post!", "success");
      } else if (result.isDenied) {
        Swal.fire("Canceled!", "", "error");
      }
    });
  };

  return (
    <div className="container alltop">
      {categorys.length == 0 ? (
        <EmptyMenu />
      ) : (
        <div className="row mt-5">
          {categorys.map((menus, i) => (
            <div key={i} className="col-lg-3 p-3">
              <div className="card h-100 shadow-sm card-menu">
                <img src={menus.image} className="card-img-top" alt="" />
                <div className="label-top shadow-sm">
                  <span className="text-white d-flex">{menus.discount}%</span>
                </div>
                <div className="card-body">
                  {localStorage.getItem("role") === "admin" ? (
                    <div>
                      <div className="d-flex justify-content-center">
                        <Link to={`/editMenu/${menus.id}`}>
                          <i className="fa-sharp fa-solid fa-pen-to-square text-warning"></i>
                        </Link>{" "}
                        {""}
                        <span>
                          <i
                            className="fa-solid fa-trash text-danger px-2"
                            onClick={() => deleteMenu(menus.id)}
                            style={{ cursor: "pointer" }}
                          ></i>
                        </span>
                      </div>
                      <hr />
                    </div>
                  ) : (
                    <div></div>
                  )}

                  <div className="mb-3">
                    <span>{convertCurrency(menus.price, "id-ID", "IDR")}</span>

                    <span className="float-end rating">
                      <ion-icon name="star"></ion-icon>
                      <ion-icon name="star"></ion-icon>
                      <ion-icon name="star"></ion-icon>
                      <ion-icon name="star"></ion-icon>
                    </span>
                  </div>
                  <h5 className="card-title">
                    <p>{menus.name}</p>
                  </h5>

                  <div className="d-grid gap-2 mt-4 mb-2">
                    <Link
                      to={`/menus/${menus.id}`}
                      className="btn bold-btn"
                      style={{ backgroundColor: "#b22222", color: "white" }}
                    >
                      Menu Detail
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
}

export default CategoryDetail;
