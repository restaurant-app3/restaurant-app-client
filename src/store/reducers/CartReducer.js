const data = {
  carts: [],
  totalCart: 0,
};

const CartReducer = (state = data, action) => {
  switch (action.type) {
    case "ADD_TO_CART":
      if (state.totalCart == 0) {
        let cart = {
          id: action.payload.id,
          quantity: 1,
          name: action.payload.name,
          image: action.payload.image,
          price: action.payload.price,
          discount: action.payload.discount,
        };
        state.carts.push(cart);
      } else {
        let check = false;
        state.carts.map((item, key) => {
          if (item.id == action.payload.id) {
            state.carts[key].quantity++;
            check = true;
          }
        });
        if (!check) {
          let _cart = {
            id: action.payload.id,
            quantity: 1,
            name: action.payload.name,
            image: action.payload.image,
            price: action.payload.price,
            discount: action.payload.discount,
          };
          state.carts.push(_cart);
        }
      }
      return {
        ...state,
        totalCart: state.totalCart + 1,
      };

    case "INCREASE_QUANTITY":
      state.totalCart++;
      state.carts[action.payload].quantity++;

      return {
        ...state,
      };

    case "DECREASE_QUANTITY":
      let quantity = state.carts[action.payload].quantity;
      if (quantity > 1) {
        state.totalCart--;
        state.carts[action.payload].quantity--;
      }

      return {
        ...state,
      };

    case "RESET_CART":
      return { carts: [], totalCart: 0 };

    case "DELETE_CART":
      let quantity_ = state.carts[action.payload].quantity;
      return {
        ...state,
        totalCart: state.totalCart - quantity_,
        carts: state.carts.filter((item) => {
          return item.id != state.carts[action.payload].id;
        }),
      };

    default:
      return state;
  }
};

export default CartReducer;
