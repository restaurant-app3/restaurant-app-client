import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { convertCurrency } from "../util/util";
import Swal from "sweetalert2";
import { instance as axios } from "../util/api";
import { useNavigate } from "react-router-dom";
import QRCode from "qrcode";

function Cart() {
  const cart = useSelector((state) => state.cart.carts);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [imageUrl, setImageUrl] = useState("");

  // menghitung total bayar
  const totalBayar = cart.reduce((acc, { price, quantity, discount }) => {
    const total = price * quantity;
    return acc + total - total * (discount / 100);
  }, 0);

  // mengurangi quantity
  const DecreaseQuantity = async (id) => {
    dispatch({ type: "DECREASE_QUANTITY", payload: id });
  };

  // menambah quantity
  const IncreaseQuantity = async (id) => {
    dispatch({ type: "INCREASE_QUANTITY", payload: id });
  };

  // menghapus cart
  const deleteToCart = async (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#b22222",
      cancelButtonColor: "green",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "The book has been successfully removed from cart.",
          showConfirmButton: false,
          timer: 1000,
        });
        dispatch({ type: "DELETE_CART", payload: id });
      }
    });
  };

  // melakukan transaksi
  const checkout = async () => {
    try {
      const data = cart.map((item) => ({
        menuId: item.id,
        totalMenus: item.quantity,
      }));

      await axios.post(
        `/api/menus/${localStorage.getItem("id")}/transactions`,
        data
      );
      dispatch({ type: "RESET_CART" });
      navigate("/sukses");
      Swal.fire({
        icon: "success",
        title: "Success!",
        text: "Successfully checkout.",
        showConfirmButton: false,
        timer: 1000,
      });
    } catch (err) {
      console.log(err);
    }
  };

  // Scann QR Code
  const generateQR = async () => {
    try {
      const response = await QRCode.toDataURL(JSON.stringify(cart));
      setImageUrl(response);
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <div>
      <div className="col-lg-12 p-3 ">
        <h4 className="mt-4">
          <b>Shopping Cart</b>
        </h4>
        <div className="table-responsive">
          <table className="table text-center mt-4">
            <thead>
              <tr
                className="table"
                style={{ backgroundColor: "#b22222", color: "white" }}
              >
                <th>Image</th>
                <th>Name</th>
                <th>Price</th>
                <th>Discount</th>
                <th>Quantity</th>
                <th>SubTotal</th>
                <th>Action</th>
              </tr>
            </thead>

            <tbody>
              {cart.map((item, key) => (
                <tr key={key}>
                  <td style={{ width: "20%" }}>
                    <img src={item.image} alt="" width={"50%"} />
                  </td>
                  <td>{item.name}</td>
                  <td>{convertCurrency(item.price, "id-ID", "IDR")}</td>
                  <td>{item.discount}%</td>
                  <td>
                    <div>
                      <span
                        onClick={() => DecreaseQuantity(key)}
                        className="text-danger"
                        role="button"
                      >
                        <i className="fa-solid fa-square-minus fs-4"></i>
                      </span>
                      <span className="m-2">{item.quantity}</span>
                      <span
                        onClick={() => IncreaseQuantity(key)}
                        className="text-success"
                        role="button"
                      >
                        <i className="fa-solid fa-square-plus fs-4"></i>
                      </span>
                    </div>
                  </td>

                  <td>
                    {convertCurrency(
                      item.price * item.quantity -
                        item.price * item.quantity * (item.discount / 100),
                      "id-ID",
                      "IDR"
                    )}
                  </td>

                  <td className="text-danger" onClick={() => deleteToCart(key)}>
                    <i
                      className="fa-solid fa-trash"
                      style={{ cursor: "pointer" }}
                    ></i>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>

      <div className="row">
        <div className="col-lg-8 p-4">
          <h4>
            <b>Cart Totals</b>
          </h4>
          <div className="card p-3 mt-4">
            {cart.map((total, i) => (
              <p key={i}>
                {total.name}
                <span className="price">
                  {convertCurrency(
                    total.price * total.quantity -
                      total.price * total.quantity * (total.discount / 100),
                    "id-ID",
                    "IDR"
                  )}
                </span>
              </p>
            ))}

            <hr className="mt-2 mb-2" />
            <p>
              <b> Total Bayar </b>
              <span className="float-end">
                <b>{convertCurrency(totalBayar, "id-ID", "IDR")} </b>
              </span>
            </p>
            <button
              onClick={generateQR}
              className="btn mt-2 w-25"
              style={{
                backgroundColor: "#b22222",
                color: "white",
                alignSelf: "center",
              }}
            >
              Checkout
            </button>
          </div>
        </div>

        {imageUrl && (
          <div className="col-lg-4 p-4">
            <h4>
              <b>Scan QR Code</b>
            </h4>
            <div className="card p-3 mt-4">
              <>
                <img
                  src={imageUrl}
                  style={{ width: "80%", alignSelf: "center" }}
                />
                {/* <a href={imageUrl} download="qrcode.png">
                  Download
                </a> */}
                <button
                  className="btn w-25"
                  onClick={checkout}
                  style={{
                    backgroundColor: "#b22222",
                    color: "white",
                    alignSelf: "center",
                  }}
                >
                  OK
                </button>
              </>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default Cart;
