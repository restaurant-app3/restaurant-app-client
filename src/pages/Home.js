import React from "react";
import ContentHome from "../components/ContentHome";
import Hero from "../components/Hero";

function Home() {
  return (
    <div className="alltop">
      <Hero />
      <ContentHome />
    </div>
  );
}

export default Home;
