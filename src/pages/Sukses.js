import React from "react";
import { Link } from "react-router-dom";

function Sukses() {
  return (
    <div className="container alltop mb-5 text-center">
      <img src="images/suksesRM.png" width="500" className="mt-4" />
      <h3 className="mt-4">Menu Successfully Ordered</h3>
      <h5>Thank you for Ordering</h5>
      <Link
        to="/"
        className="btn mt-4 px-4 justify-content-sm-center"
        style={{
          backgroundColor: "#b22222",
          color: "white",
        }}
      >
        Back
      </Link>
    </div>
  );
}

export default Sukses;
