import { Route, Routes } from "react-router-dom";
import "./index.css";
import AddCategory from "./pages/AddCategory";
import AddMenu from "./pages/AddMenu";
import EditMenu from "./pages/EditMenu";
import Footer from "./components/Footer";
import MyNavbar from "./components/MyNavbar";
import PrivateRoute from "./components/PrivateRoute";
import Carts from "./pages/Carts";
import CategoryDetail from "./pages/CategoryDetail";
import Home from "./pages/Home";
import Login from "./pages/Login";
import MenuDetail from "./pages/MenuDetail";
import Menus from "./pages/Menus";
import Profile from "./pages/Profile";
import Register from "./pages/Register";
import Sukses from "./pages/Sukses";
import PageNotFound from "./pages/PageNotFound";

function App() {
  return (
    <div className="App">
      <MyNavbar />
      <Routes>
        <Route path="/" element={<Home />} />

        <Route
          path="/menus"
          element={
            <PrivateRoute>
              <Menus />
            </PrivateRoute>
          }
        />

        <Route
          path="/menus/add"
          element={
            <PrivateRoute>
              <AddMenu />
            </PrivateRoute>
          }
        />

        <Route
          path="/categorys/add"
          element={
            <PrivateRoute>
              <AddCategory />
            </PrivateRoute>
          }
        />

        <Route
          path="/menus/:id"
          element={
            <PrivateRoute>
              <MenuDetail />
            </PrivateRoute>
          }
        />

        <Route
          path="/editMenu/:id"
          element={
            <PrivateRoute>
              <EditMenu />
            </PrivateRoute>
          }
        />

        <Route
          path="/categorys/:id"
          element={
            <PrivateRoute>
              <CategoryDetail />
            </PrivateRoute>
          }
        />

        <Route
          path="/profile"
          element={
            <PrivateRoute>
              <Profile />
            </PrivateRoute>
          }
        />

        <Route
          path="/sukses"
          element={
            <PrivateRoute>
              <Sukses />
            </PrivateRoute>
          }
        />

        <Route
          path="/carts"
          element={
            <PrivateRoute>
              <Carts />
            </PrivateRoute>
          }
        />

        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/*" element={<PageNotFound />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
