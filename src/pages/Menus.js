import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import Menu from "../components/Menu";
import ScrollToTop from "../components/ScrollToTop";
import { instance as axios } from "../util/api";

function Menus() {
  const [menus, setMenus] = useState([]);
  const [category, setCategory] = useState([]);

  const fetchMenus = async () => {
    try {
      const { data, status } = await axios.get(`/api/menus/`);
      if (status === 200) {
        setMenus(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchMenus();
    fetchCategory();
  }, []);

  const fetchCategory = async () => {
    try {
      const { data, status } = await axios.get(`/api/categorys/`);
      if (status === 200) {
        setCategory(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const removeCategory = async (idCategory) => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/api/categorys/delete/${idCategory}`);
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your category!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled!",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  return (
    <div className="container mb-5" style={{ marginTop: "100px" }}>
      <div className="section-title">
        {localStorage.getItem("role") === "admin" ? (
          <div>
            <Link
              to="/menus/add"
              className="btn fw-bold text-white"
              style={{
                backgroundColor: "#b22222",
              }}
            >
              <i className="fa-solid fa-circle-plus"></i> Menu
            </Link>{" "}
            {""}
            <Link
              to="/categorys/add"
              className="btn fw-bold text-white"
              style={{
                backgroundColor: "#b22222",
              }}
            >
              <i className="fa-solid fa-circle-plus"></i> Category
            </Link>
          </div>
        ) : (
          <div></div>
        )}

        <h2 className="mt-3">
          Our Delicious <span className="text-danger">Foods</span>
        </h2>
      </div>

      <p className="section-text">
        Food is any substance consumed to provide nutritional support for an
        organism.
      </p>

      {localStorage.getItem("role") === "admin" ? (
        <div className="row mb-5 justify-content-center mx-auto w-75">
          {category.map((cat, i) => (
            <div key={i} className="col-lg-2 px-1 dropdown">
              <button
                className="btn w-100 mb-2 justify-content-center fw-bold dropdown-toggle text-danger"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                style={{
                  border: " 1px solid #b22222",
                }}
              >
                {cat.name}
              </button>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <Link
                  className="dropdown-item isi-dr"
                  to={`/categorys/${cat.id}`}
                >
                  Detail Category
                </Link>
                <button
                  className="dropdown-item isi-dr"
                  onClick={() => removeCategory(cat.id)}
                >
                  Delete
                </button>
              </div>
            </div>
          ))}
        </div>
      ) : (
        <div className="row mt-3 mb-5 justify-content-center mx-auto w-75">
          {category.map((cat, i) => (
            <div key={i} className="col-lg-2 px-1">
              <Link
                to={`/categorys/${cat.id}`}
                className="btn btn-outline-danger w-100 mb-2 justify-content-center"
              >
                {cat.name}
              </Link>
            </div>
          ))}
        </div>
      )}

      <div className="row" style={{ marginTop: "80px" }}>
        {menus.map((menu, i) => (
          <div key={i} className="col-lg-3 p-3">
            <Menu key={menu.id} menu={menu} setMenus={setMenus} />
          </div>
        ))}
      </div>
      <ScrollToTop />
    </div>
  );
}

export default Menus;
