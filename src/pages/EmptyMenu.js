import React from "react";
import { Link } from "react-router-dom";

function EmptyMenu() {
  return (
    <div className="container alltop mb-5 text-center">
      <img src="/images/pageNotFoundRM.png" width="500" className="mt-4" />
      <h3 className="mt-4">Oops! Menu not Available</h3>
      <p>Menu is not available in this category</p>
      <Link
        to="/"
        className="btn mt-4 px-4 justify-content-sm-center text-white fw-bold"
        style={{
          backgroundColor: "#b22222",
        }}
      >
        Back
      </Link>
    </div>
  );
}

export default EmptyMenu;
