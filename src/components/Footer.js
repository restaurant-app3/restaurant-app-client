import React from "react";
import { Link } from "react-router-dom";

const Footer = () => {
  return (
    <footer className="text-white text-center text-lg-start">
      <div className="container p-4">
        <div className="row">
          <div className="col-lg-3">
            <h5 className="text-uppercase">Mala's Resto</h5>
            <hr />
            <p>
              Financial experts support or help you to to find out which way you
              can raise your funds more.
            </p>
          </div>

          <div className="col-lg-3 px-5 mb-4">
            <h5 className="text-uppercase">Contact</h5>
            <hr />

            <ul className="list-unstyled mb-0">
              <li>
                <i className="bi bi-whatsapp"></i> +62 813-8822-3105
              </li>
              <li>
                <i className="bi bi-facebook"></i> Mala's_resto
              </li>
              <li>
                <i className="bi bi-instagram"></i> resto_mala's
              </li>
            </ul>
          </div>

          <div className="col-lg-3 px-4 mb-3">
            <h5 className="text-uppercase">Opening Hours</h5>
            <hr />

            <ul className="list-unstyled mb-0">
              <li>Monday-Friday: 08:00-22:00</li>
              <li>Tuesday 4PM: Till Mid Night</li>
              <li>Saturday: 10:00-16:00</li>
            </ul>
          </div>

          <div className="col-lg-3">
            <h5 className="text-uppercase">Object</h5>
            <hr />
            <p className="text-justify">
              Jl. Kemantren Raya No.5, RT.02/RW.04, Wonosari, Kec. Ngaliyan,
              Kota Semarang, Jawa Tengah 50186
            </p>
          </div>
        </div>
      </div>

      <div
        className="text-center p-3"
        style={{ backgroundColor: "rgba(0, 0, 0, 0.1)" }}
      >
        © 2022 Copyright : {""}
        <Link className="text-white" to="/">
          Mala's Restaurant App
        </Link>
      </div>
    </footer>
  );
};

export default Footer;
