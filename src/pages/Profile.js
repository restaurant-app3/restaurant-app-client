import React from "react";

function Profile() {
  return (
    <div className="container alltop mb-5 " style={{ marginTop: "110px" }}>
      <div className="text-center">
        <img
          style={{
            width: "200px",
            marginBottom: "-100px",
            border: "3px solid #b22222",
          }}
          src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRT_bZVtu-PddY_qhswZvb8mjbcupdRazjY_w&usqp=CAU"
          alt="Circle Image"
          className="img-raised rounded-circle img-fluid header-profile"
        />
        <div
          className="header-profile p-4 rounded"
          style={{ border: "3px solid #b22222" }}
        >
          <h3 style={{ marginTop: "100px" }}>admin@gmail.com</h3>
          <h6 className="text-danger">role : admin</h6>
          <p className="w-75 mt-4 mx-auto">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolores
            ipsa minima quis odit officiis sed incidunt quidem suscipit? Odio in
            at mollitia quibusdam iste eaque sequi eius deserunt quae explicabo!
          </p>
        </div>
      </div>
    </div>
  );
}

export default Profile;
