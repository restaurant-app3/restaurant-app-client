import React from "react";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import Searching from "./Searching";

function MyNavbar() {
  const navigate = useNavigate();
  const totalCart = useSelector((state) => state.cart.totalCart);

  const signOut = () => {
    localStorage.removeItem("token");
    navigate("/login");
  };

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light p-2 position-fixed">
        <div className="container">
          <Link
            className="nav-link navbar-brand text-white fs-5 fw-bold"
            to="/"
          >
            <img
              src="images/restaurant.jpg"
              alt=""
              style={{ width: "40px", borderRadius: "100%" }}
            />{" "}
            {""}
            Mala's Resto
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link className="nav-link text-white fw-bold" to="/">
                  Home
                </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link text-white fw-bold" to="/menus">
                  Menu
                </Link>
              </li>
            </ul>

            {localStorage.getItem("token") && (
              <ul className="navbar-nav justify-content-start mx-3">
                <Searching />
              </ul>
            )}

            {localStorage.getItem("token") &&
            localStorage.getItem("role") === "user" ? (
              <ul className="navbar-nav d-flex flex-row mt-3 mt-lg-0">
                <Link className="nav-link text-white p-1" to="/carts">
                  <i
                    className="fa-solid fa-cart-plus"
                    style={{ fontSize: "25" }}
                  ></i>
                  <span className="position-absolute top-10 start-90 translate-middle badge rounded-pill bg-warning">
                    {totalCart}
                  </span>
                </Link>
              </ul>
            ) : (
              <div></div>
            )}

            <div className="d-flex input-group w-auto ms-lg-3 my-3 my-lg-0">
              {localStorage.getItem("token") ? (
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  <li className="nav-item dropdown">
                    <Link
                      className="nav-link dropdown-toggle text-white"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      <i className="fa-solid fa-user text-white"></i>
                    </Link>
                    <ul
                      className="dropdown-menu"
                      aria-labelledby="navbarDropdown"
                    >
                      <li>
                        <Link className="dropdown-item isi-dr" to="/profile">
                          Profile
                        </Link>
                      </li>
                      <li>
                        <button
                          className="dropdown-item isi-dr"
                          onClick={signOut}
                        >
                          Logout
                        </button>
                      </li>
                    </ul>
                  </li>
                </ul>
              ) : (
                <ul className="navbar-nav d-flex flex-row mt-3 mt-lg-0">
                  <Link className="nav-link text-white fw-bold" to="/login">
                    Login
                  </Link>
                  <Link className="nav-link text-white fw-bold" to="/register">
                    Register
                  </Link>
                </ul>
              )}
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default MyNavbar;
