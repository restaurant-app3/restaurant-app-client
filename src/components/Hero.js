import React from "react";
import { Link } from "react-router-dom";

function Hero() {
  return (
    <div className="container mt-5 p-4">
      {/* Desktop  */}
      <div className="d-none d-md-block">
        <div className="row">
          <div className="col-md-6">
            <div className="d-flex h-100">
              <div className="justify-content-center align-self-center">
                <h2>
                  <strong>Welcome to,</strong>
                  <br />
                  Restaurant App <br />
                </h2>
                <br />
                <p className="descHero">
                  A restaurant is a business that prepares and serves food and
                  drinks to customers. Meals are generally served and eaten on
                  the premises, but many restaurants also offer take-out and
                  food delivery services. Restaurants vary greatly in appearance
                  and offerings, including a wide variety of cuisines and
                  service models ranging from inexpensive fast-food restaurants
                  and cafeterias to mid-priced family restaurants, to
                  high-priced luxury establishments.
                </p>
                <br />

                <Link
                  className="btn fw-bold text-white"
                  to="/menus"
                  style={{
                    backgroundColor: "#B22222",
                  }}
                >
                  List Menus <i className="bi bi-arrow-right"></i>
                </Link>
              </div>
            </div>
          </div>
          <div className="col-md-6 p-5">
            <img src="images/heroBannerRM.png" width="100%" />
          </div>
        </div>
      </div>

      {/* Mobile  */}
      <div className="d-sm-block d-md-none">
        <div className="row mt-5">
          <div className="col-md-6 mb-3 p-5">
            <img src="images/heroBannerRM.png" width="100%" />
          </div>
          <div className="col-md-6">
            <div className="d-flex h-100">
              <div className="justify-content-center align-self-center">
                <h2>
                  <strong>Restaurant App,</strong>
                  <br />
                  in Your Gadget
                </h2>
                <p className="descHero">
                  A book is a collection / collection of papers or sheets that
                  are written or contain writing. These materials can be in the
                  form of pieces made of wood, paper and even elephant ivory.
                  This collection is collected or bound together at one end and
                  contains writing, pictures or patches. Each side of a sheet of
                  paper in a book is called a page.
                </p>

                <a
                  className="btn fw-bold text-white"
                  to="/books"
                  style={{
                    backgroundColor: "#B22222",
                  }}
                >
                  List Books <i className="fa-solid fa-arrow-right-long"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Hero;
