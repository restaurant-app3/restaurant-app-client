import { instance as axios } from "../util/api";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  deleteObject,
  getDownloadURL,
  ref,
  uploadBytes,
} from "firebase/storage";
import Swal from "sweetalert2";
import { storage } from "../Firebas";

function EditMenu() {
  const navigate = useNavigate();
  const { id } = useParams();

  const [name, setName] = useState("");
  const [image, setImage] = useState("");
  const [editImage, setEditImage] = useState([]);
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [discount, setDiscount] = useState("");

  const updateMenu = async (downloadUrl) => {
    try {
      // untuk mengirimkan file multipart ke server
      const data = {
        name: name,
        image: downloadUrl,
        description: description,
        price: price,
        discount: discount,
      };

      Swal.fire({
        title: "Do you want to save changes?",
        icon: "question",
        showDenyButton: true,
        confirmButtonText: "Save",
        denyButtonText: `Cancel`,
      }).then((result) => {
        if (result.isConfirmed) {
          console.log(data);
          axios.put(`/api/menus/update/${id}`, data);
          const storageRef = ref(storage, `images/${editImage.image}`);

          // disinilah teman-teman menghapus image sebelumnya!
          deleteObject(storageRef);

          navigate("/menus");
          Swal.fire({
            icon: "success",
            title: "Saved!",
            text: "Menu changed successfully!",
            showConfirmButton: false,
            timer: 1000,
          });
        } else if (result.isDenied) {
          Swal.fire({
            icon: "info",
            title: "",
            text: "Changes are not saved!",
            showConfirmButton: false,
            timer: 1000,
          });
        }
      });
    } catch (err) {
      console.log(err);
    }
  };

  const submit = () => {
    // untuk storage
    const storageRef = ref(storage, `images/${image.name}`);

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadUrl) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          updateMenu(downloadUrl);
          console.log(downloadUrl);
        });
      });
  };

  const getDataById = async () => {
    const { data } = await axios.get(`/api/menus/${id}`);
    setName(data.name);
    setImage(data.image);
    setEditImage(data);
    setDescription(data.description);
    setPrice(data.price);
    setDiscount(data.discount);
    // console.log(data);
  };

  useEffect(() => {
    getDataById();
  }, [id]);

  return (
    <div className="container alltop mb-5">
      <h2 className="text-center mb-3">
        <i className="fa-sharp fa-solid fa-utensils"></i> {""}
        <span className="text-danger">Update Menu </span>
      </h2>

      <form className="card p-4 mt-4">
        <div className="form-group">
          <label>
            <strong>Name</strong>
          </label>
          <input
            type="text"
            className="form-control"
            placeholder="Title"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>

        <div className="form-group">
          <label>
            <strong>Image</strong>
          </label>
          <input
            type="file"
            className="form-control"
            placeholder="Image"
            onChange={(e) => setImage(e.target.files[0])}
          />
        </div>

        <div className="form-group">
          <label>
            <strong>Description</strong>
          </label>
          <textarea
            type="text"
            className="form-control"
            placeholder="Description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            rows="3"
          />
        </div>

        <div className="form-row d-flex">
          <div className="form-group col-md-6">
            <label>
              <strong>Price</strong>
            </label>
            <input
              type="number"
              className="form-control"
              placeholder="Price"
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </div>

          <div className="form-group col-md-6">
            <label>
              <strong>Discount</strong>
            </label>
            <input
              type="number"
              className="form-control"
              placeholder="Discount"
              value={discount}
              onChange={(e) => setDiscount(e.target.value)}
            />
          </div>
        </div>
      </form>

      <button
        onClick={submit}
        className="btn btn-block mt-3 w-15 text-white"
        style={{ backgroundColor: "#B22222" }}
      >
        Save
      </button>
    </div>
  );
}

export default EditMenu;
