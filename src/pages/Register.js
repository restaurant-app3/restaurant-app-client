import { instance as axios } from "../util/api";
import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";

function Register() {
  const location = useLocation();
  const navigate = useNavigate();

  const [userRegister, setUserRegister] = useState({
    email: "",
    password: "",
    role: "",
  });

  const handleOnChange = (e) => {
    setUserRegister((currUser) => {
      return { ...currUser, [e.target.id]: e.target.value };
    });
  };

  const signUp = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(`/register`, userRegister);

      if (status === 200) {
        localStorage.setItem("token", data.token);

        if (location.state) {
          navigate(`${location.state.from.pathname}`);
        } else {
          navigate("/");
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className="container alltop mb-4 p-5">
      <div className="card p-5" style={{ border: "none" }}>
        <div className="row d-flex justify-content-center align-items-center">
          <div className="col-lg-6 p-0">
            <img src="images/login.png" style={{ width: "100%" }} />
          </div>
          <div className="col-lg-6 col-xl-4">
            <h3
              className="text-center mb-3 fw-bold"
              style={{ color: "#B22222" }}
            >
              Register
            </h3>
            <form className="card p-4">
              <div className="form-outline mb-1">
                <label className="form-label">Email</label>
                <input
                  type="email"
                  id="email"
                  className="form-control"
                  onChange={handleOnChange}
                  value={userRegister.email}
                  placeholder="Enter email address"
                />
              </div>

              <div className="form-outline mb-1">
                <label className="form-label">Password</label>
                <input
                  type="password"
                  id="password"
                  className="form-control"
                  onChange={handleOnChange}
                  value={userRegister.password}
                  placeholder="Enter password"
                />
              </div>

              <div className="form-outline mb-1">
                <label className="form-label">Role</label>
                <input
                  type="text"
                  id="role"
                  className="form-control"
                  onChange={handleOnChange}
                  value={userRegister.role}
                  placeholder="Enter role"
                />
              </div>

              <div className="text-center mt-4">
                <button
                  type="button"
                  className="btn btn-color mb-2 w-100 text-white fw-bold"
                  style={{ backgroundColor: "#B22222" }}
                  onClick={signUp}
                >
                  Register
                </button>
                <p className="small fw-bold mt-2 mb-0">
                  Already have an account? {""}
                  <Link className="link-danger" to="/login">
                    Login
                  </Link>
                </p>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;
