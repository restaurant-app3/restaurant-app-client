import { instance as axios } from "../util/api";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import { convertCurrency } from "../util/util";

function MenuDetail() {
  const [menus, setMenus] = useState({
    name: "",
    discount: "",
    image: null,
    price: "",
  });

  const dispatch = useDispatch();
  const { id } = useParams();

  const fetchMenusId = async () => {
    try {
      const { data, status } = await axios.get(`/api/menus/${id}`);
      if (status === 200) {
        setMenus(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchMenusId();
  }, []);

  const addToCart = async () => {
    Swal.fire({
      icon: "success",
      title: "Good job!",
      text: "Menu successfully added to cart",
      showConfirmButton: false,
      timer: 1000,
    });
    dispatch({ type: "ADD_TO_CART", payload: menus });
  };

  const removePost = async () => {
    await Swal.fire({
      title: "Do you want to delete the Post?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Delete",
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`/api/menus/delete/${menus.id}`);
        Swal.fire({
          icon: "success",
          title: "Deleted!",
          text: "Successfully deleted your post!",
          showConfirmButton: false,
          timer: 1000,
        });
      } else if (result.isDenied) {
        Swal.fire({
          icon: "error",
          title: "Canceled!",
          text: "",
          showConfirmButton: false,
          timer: 1000,
        });
      }
    });
  };

  return (
    <div className="container alltop mb-5">
      <div className="row">
        <div className="col-lg-6 p-3 mx-auto">
          <div className="card h-100 shadow-sm card-menu p-3">
            <img src={menus.image} className="card-img-top" alt="" />
            <div className="label-top-detail shadow-sm">
              <span className="text-white d-flex justify-content-center">
                {menus.discount}%
              </span>
            </div>
            <div className="card-body">
              {localStorage.getItem("role") === "admin" ? (
                <div>
                  <div className="d-flex justify-content-start">
                    <Link to={`/editMenu/${menus.id}`}>
                      <i className="fa-sharp fa-solid fa-pen-to-square text-warning"></i>
                    </Link>{" "}
                    {""}
                    <span>
                      <i
                        className="fa-solid fa-trash text-danger px-2"
                        onClick={removePost}
                        style={{ cursor: "pointer" }}
                      ></i>
                    </span>
                  </div>
                </div>
              ) : (
                <div></div>
              )}

              <h5 className="card-title text-center fs-4">
                <p>{menus.name}</p>
              </h5>
              <hr className="mt-0 mb-4 hr" />
              <div className="clearfix mb-3">
                <span className="fw-bold">
                  {convertCurrency(menus.price, "id-ID", "IDR")}
                </span>

                <span className="float-end rating">
                  <ion-icon name="star"></ion-icon>
                  <ion-icon name="star"></ion-icon>
                  <ion-icon name="star"></ion-icon>
                  <ion-icon name="star"></ion-icon>
                </span>
              </div>
              <p className="menuDesc">{menus.description}</p>

              {localStorage.getItem("role") === "user" ? (
                <div className="d-grid gap-2 mt-4">
                  <button
                    onClick={addToCart}
                    className="btn bold-btn text-white"
                    style={{ backgroundColor: "#b22222" }}
                  >
                    Add to Cart
                  </button>
                </div>
              ) : (
                <div></div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MenuDetail;
