import React from "react";
import { Link } from "react-router-dom";

function EmptyCart() {
  return (
    <div className="container alltop mb-5 text-center">
      <img src="/images/emptyCartRM.png" width="500" className="mt-4" />
      <h3 className="mt-4">Oops! Cart Not Found</h3>
      <p>Please add the menu to the cart first</p>
      <Link
        to="/menus"
        className="btn mt-4 px-4 justify-content-sm-center text-white fw-bold"
        style={{
          backgroundColor: "#b22222",
        }}
      >
        Back
      </Link>
    </div>
  );
}

export default EmptyCart;
