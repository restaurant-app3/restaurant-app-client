import React from "react";

function ContentHome() {
  return (
    <div className="container mb-5">
      <div className="row promo-list has-scrollbar">
        <div className="col-lg-3">
          <div className="promo-card">
            <h3 className="h3 card-title">Maxican Pizza</h3>

            <p className="card-text">
              Food is any substance consumed to provide nutritional support for
              an organism.
            </p>

            <img
              src="images/promo-1.png"
              loading="lazy"
              alt="Maxican Pizza"
              className="w-100 card-banner"
            />
          </div>
        </div>

        <div className="col-lg-3 promo-item">
          <div className="promo-card">
            <h3 className="h3 card-title">Soft Drinks</h3>

            <p className="card-text">
              Food is any substance consumed to provide nutritional support for
              an organism.
            </p>

            <img
              src="images/promo-2.png"
              loading="lazy"
              alt="Soft Drinks"
              className="w-100 card-banner"
            />
          </div>
        </div>

        <div className=" col-lg-3 promo-item">
          <div className="promo-card">
            <h3 className="h3 card-title">French Fry</h3>

            <p className="card-text">
              Food is any substance consumed to provide nutritional support for
              an organism.
            </p>

            <img
              src="images/promo-3.png"
              loading="lazy"
              alt="French Fry"
              className="w-100 card-banner"
            />
          </div>
        </div>

        <div className="col-lg-3 promo-item">
          <div className="promo-card">
            <h3 className="h3 card-title">Burger Kingo</h3>

            <p className="card-text">
              Food is any substance consumed to provide nutritional support for
              an organism.
            </p>

            <img
              src="images/promo-4.png"
              loading="lazy"
              alt="Burger Kingo"
              className="w-100 card-banner"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default ContentHome;
