import React, { useEffect, useState } from "react";
import { instance as axios } from "../util/api";
import { Link } from "react-router-dom";

function Searching() {
  const [query, setQuery] = useState([]);
  const [inputText, setInputText] = useState("");

  const getSearching = async (txt) => {
    try {
      const { data, status } = await axios.get(
        `api/menus/search?name=${txt}`,
        {}
      );
      if (status === 200) {
        setQuery(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  let inputSearch = (e) => {
    var lowerCase = e.target.value.toLowerCase();
    setInputText(lowerCase);
    getSearching(lowerCase);
    e.preventDefault();
  };

  useEffect(() => {
    getSearching();
  }, []);

  return (
    <div>
      <div className="search">
        <div className="d-flex">
          <input
            className="form-control"
            type="text"
            placeholder="Search name menu..."
            onChange={inputSearch}
          />
        </div>
        {inputText === "" ? (
          <div></div>
        ) : (
          <ul className="list-group position-absolute">
            {query.map((search, index) => {
              return (
                <Link
                  to={`/menus/${search.id}`}
                  className="post-book list-group-item list-group-item-action"
                  key={index}
                >
                  <div className="d-flex">
                    <img
                      src={search.image}
                      alt=""
                      width="75px"
                      className="border border-light"
                    />
                    <p className="p-2 mt-1">{search.name}</p>
                  </div>
                </Link>
              );
            })}
          </ul>
        )}
      </div>
    </div>
  );
}

export default Searching;
