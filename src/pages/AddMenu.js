import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { storage } from "../Firebas";
import { instance as axios } from "../util/api";

function AddMenu() {
  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [image, setImage] = useState(null);
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [category, setCategory] = useState([]);
  const [selectCategory, setSelectCategory] = useState("");
  const [discount, setDiscount] = useState("");

  const AddMenus = async (downloadUrl) => {
    try {
      // untuk mengirimkan file multipart ke server
      const formData = {
        name: name,
        image: downloadUrl,
        description: description,
        category: { id: selectCategory },
        price: price,
        discount: discount,
      };
      console.log(formData);
      await axios.post(`/api/menus/add`, formData);
      Swal.fire({
        icon: "success",
        title: "Good job!",
        text: "Menu added successfully",
        showConfirmButton: false,
        timer: 1000,
      });
      navigate("/menus");
    } catch (err) {
      console.log(err);
    }
  };

  const submit = () => {
    // untuk storage
    const storageRef = ref(storage, `images/${image.name}`);

    // 'file' comes from the Blob or File API
    uploadBytes(storageRef, image)
      .then((snapshot) => {
        console.log("Upload berhasil");
        console.log(snapshot);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        getDownloadURL(storageRef).then((downloadUrl) => {
          // url nya ini nanti untuk dikirim ke server API yang dimasukkan ke database
          AddMenus(downloadUrl);
          console.log(downloadUrl);
        });
      });
  };

  const save = (e) => {
    e.preventDefault();
    submit();
  };

  const fetchCategory = async () => {
    try {
      const { data, status } = await axios.get(`/api/categorys/`);
      if (status === 200) {
        setCategory(data);
      }
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    fetchCategory();
  }, []);

  return (
    <div className="container alltop mb-5">
      <h2 className="text-center mb-3">
        <i className="fa-sharp fa-solid fa-utensils"></i> {""}
        <span className="text-danger">Add Menu </span>
      </h2>

      <form className="card p-4 mt-4">
        <div className="form-group">
          <label>
            <strong>Name</strong>
          </label>
          <input
            type="text"
            className="form-control"
            placeholder="Title"
            required
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>

        <div className="form-group">
          <label>
            <strong>Description</strong>
          </label>
          <textarea
            type="text"
            className="form-control"
            placeholder="Description"
            required
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            rows="3"
          />
        </div>

        <div className="form-row d-flex">
          <div className="form-group col-md-6">
            <label>
              <strong>Image</strong>
            </label>
            <input
              type="file"
              className="form-control"
              placeholder="cover"
              accept="image/png, image/jpeg, image/jpg"
              required
              onChange={(e) => setImage(e.target.files[0])}
            />
          </div>

          <div className="form-group col-md-6">
            <label>
              <strong>Category</strong>
            </label>
            <select
              className="form-select"
              aria-label="Default select example"
              name="category"
              required
              value={selectCategory}
              onChange={(e) => {
                setSelectCategory(e.target.value.toString());
              }}
            >
              <option selected disabled value={""}>
                Select Category
              </option>
              {category.map((cat, index) => (
                <option value={cat.id} key={index}>
                  {cat.name}
                </option>
              ))}
            </select>
          </div>
        </div>

        <div className="form-row d-flex">
          <div className="form-group col-md-6">
            <label>
              <strong>Price</strong>
            </label>
            <input
              type="number"
              className="form-control"
              placeholder="Price"
              required
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </div>

          <div className="form-group col-md-6">
            <label>
              <strong>Discount</strong>
            </label>
            <input
              type="number"
              className="form-control"
              placeholder="Discount"
              required
              value={discount}
              onChange={(e) => setDiscount(e.target.value)}
            />
          </div>
        </div>
      </form>

      <button
        onClick={save}
        className="btn btn-block mt-3 w-15"
        style={{ backgroundColor: "#B22222", color: "white" }}
      >
        Save
      </button>
    </div>
  );
}

export default AddMenu;
