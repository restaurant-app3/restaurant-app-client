import React from "react";
import { Navigate, useLocation } from "react-router-dom";

function PrivateRoute({ children }) {
  const location = useLocation();

  if (!localStorage.getItem("token")) {
    //kalo token di UI belum ada
    return <Navigate to="/login" state={{ from: location }} />;
  }

  //kalo token sudah ada di UI
  return children;
}

export default PrivateRoute;
