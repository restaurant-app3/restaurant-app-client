import React from "react";
import { useSelector } from "react-redux";
import Cart from "../components/Cart";
import EmptyCart from "./EmptyCart";

function Carts() {
  const totalCart = useSelector((state) => state.cart.totalCart);

  return (
    <div className="container mt-5 mb-5">
      {totalCart == 0 ? <EmptyCart /> : <Cart />}
    </div>
  );
}

export default Carts;
