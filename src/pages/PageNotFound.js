import React from "react";
import { Link } from "react-router-dom";

function PageNotFound() {
  return (
    <div className="container alltop mb-5 text-center">
      <img src="/images/pageNotFoundRM.png" width="500" className="mt-4" />
      <h3 className="mt-4">Oops! Page Not Found</h3>
      <p>The page you are looking for was not found </p>
      <Link
        to="/"
        className="btn mt-4 px-4 text-white justify-content-sm-center fw-bold"
        style={{
          backgroundColor: "#b22222",
        }}
      >
        Back
      </Link>
    </div>
  );
}

export default PageNotFound;
