const convertCurrency = (value, codeCountry, currency) => {
  return value.toLocaleString(codeCountry, {
    style: "currency",
    currency: currency,
  });
};

export { convertCurrency };
